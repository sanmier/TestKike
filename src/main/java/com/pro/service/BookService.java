package com.pro.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.Book;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2022/3/10
 */
public interface BookService extends IService<Book> {

}
