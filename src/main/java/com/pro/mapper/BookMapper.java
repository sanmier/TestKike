package com.pro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.Book;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2022/3/10
 */
public interface BookMapper extends BaseMapper<Book> {
}
