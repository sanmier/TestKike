package com.pro.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.entity.Book;
import org.junit.jupiter.api.Test;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2022/3/10
 */
@RestController
public class BookController{

    @Test
    public void getQueryWrapper(){
        QueryWrapper<Book> eq = new QueryWrapper<Book>().eq("NAME", "开端");
        System.out.println(eq);

    }
}
