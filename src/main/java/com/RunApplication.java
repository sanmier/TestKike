package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2022/3/10
 */
@MapperScan("com.pro.mapper")
@EnableScheduling
@SpringBootApplication
public class RunApplication {
    public static void main(String[] args){
        SpringApplication.run(RunApplication.class,args);
    }
}
