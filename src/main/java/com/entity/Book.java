package com.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: liweicheng
 * @Date: 2021/7/13 14:15
 */
@Data
@TableName(value = "BOOK")
public class Book implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id")
    private Integer id;

    private String name;

    private Date publishDt;

    @TableField(exist = false)
    private String publishFactory;

    public Book() {
    }

    public Book(Integer id, String name, Date publishDt) {
        this.id = id;
        this.name = name;
        this.publishDt = publishDt;
        this.publishFactory = publishFactory;
    }
}
