package com.entity;

import lombok.Getter;

@Getter
public enum InteractiveType {
    /**
     * 不做处理
     */
    NOTHING("1"),
    /**
     * 邮件提醒
     */
    EMAIL("2"),
    /**
     * 电话提醒
     */
    PHONE("3");

    private String method;

    InteractiveType(String method) {
        this.method = method;
    }
}