package com.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TagDTO implements Serializable {
    private Integer tagId;
    private String tagName;
    //情感值 1=正向 0=中性 -1=负向
    private Integer affection;
    //标签关联数量
    private Integer relatedAmount;
}
