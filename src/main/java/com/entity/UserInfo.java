package com.entity;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * @Author: liweicheng
 * @Date: 2021/7/13 14:15
 */
public class UserInfo {

    @ExcelProperty(index = 0)
    private String name;

    @ExcelProperty(index = 1)
    private int age;

    @ExcelProperty(index = 2)
    private String address;

}
