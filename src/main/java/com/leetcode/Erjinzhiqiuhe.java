package com.leetcode;

import java.math.BigInteger;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/12/15
 * 给你两个二进制字符串，返回它们的和（用二进制表示）。
 * 输入为 非空 字符串且只包含数字 1 和 0。
 */
public class Erjinzhiqiuhe {
    public static void main(String[] args) {
//        System.out.println(addBinary("101", "11"));
        System.out.println(addBinary1("101", "11"));
    }
    //大树类有相关接口
    public static String addBinary1(String a, String b) {
        BigInteger b1 = new BigInteger(a, 2);
        BigInteger b2 = new BigInteger(b, 2);
        return b1.add(b2).toString(2);
    }
    public static String addBinary(String a, String b) {
        if(a == null || a.length() == 0) {
            return b;
        }
        if(b == null || b.length() == 0) {
            return a;
        }

        StringBuilder stb = new StringBuilder();
        int i = a.length() - 1;
        int j = b.length() - 1;
        // 进位
        int c = 0;
        while(i >= 0 || j >= 0) {
            if(i >= 0) {
                c += a.charAt(i --) - '0';
            }
            if(j >= 0) {
                c += b.charAt(j --) - '0';
            }
            stb.append(c % 2);
            c >>= 1;
        }

        String res = stb.reverse().toString();
        return c > 0 ? '1' + res : res;
    }
}
