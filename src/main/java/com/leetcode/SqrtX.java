package com.leetcode;

import org.junit.jupiter.api.Test;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2022/1/18
 * 给你一个非负整数x,计算并返回x的算术平方根。
 * 由于返回类型是整数，结果只保留整数部分，小数部分将被舍去 。
 * 注意：不允许使用任何内置指数函数和算符，例如 pow(x,0.5) 或者 x**0.5。
 * --算数平方根指的是非负数平方根。
 */
public class SqrtX {
    int y;

    @Test
    public void test(){
        int x = 16;
        int i = mySqrt(x);
        System.out.println(i);
    }

    public int mySqrt(int x) {
        y = x;
        if (x == 0){
            return 0;
        }
        return ((int) sqrts(x));
    }
    public double sqrts(double x){
        double res = (x + y/x) / 2;
        if (res == x){
            return x;
        }else {
            return sqrts(res);
        }
    }
}
