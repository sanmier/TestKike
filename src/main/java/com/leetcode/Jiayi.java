package com.leetcode;

import org.springframework.web.client.RestTemplate;

/**
 * @Author: liweicheng
 * @Date: 2021/7/13 14:15
 * 给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。
 * 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
 * 你可以假设除了整数 0 之外，这个整数不会以零开头。
 */
public class Jiayi {
    public static void main(String[] args) {

        RestTemplate restTemplate = new RestTemplate();
        int[] digits = {1,2,3,4,5,9};
        plusOne(digits);
    }

//    //存在结果溢出问题
//    public static int[] plusOne(int[] digits) {
//        int sum = 0;
//        for (int i = 0; i < digits.length; i++) {
//            int length = digits.length;
//            int num = Math.multiplyExact(digits[i], (int) Math.pow(10,length-(i+1)));
//            sum += num;
//            System.out.println(sum);
//        }
//        sum ++;
//        String ss = String.valueOf(sum);
//        int[] a = new int[ss.length()];
//        for(int i = 0; i < ss.length(); i++) {
//        //先由字符串转换成char,再转换成String,然后Integer
//            a[i] = Integer.parseInt(String.valueOf(ss.charAt(i)));
//        }
//
//        return a;
//    }
    public static int[] plusOne(int[] digits) {
        int len = digits.length;
        for (int i = len - 1; i >= 0; i--) {
            digits[i] = (digits[i] + 1) % 10;
            if (digits[i] != 0) {
                return digits;
            }
        }
        digits = new int[len + 1];
        digits[0] = 1;
        return digits;
    }
}
