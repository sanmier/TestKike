package com.leetcode;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2022/11/21
 *
 * 题目：给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target，写一个函数搜索 nums 中的 target
 * 如果目标值存在返回下标，否则返回 -1
 *
 * 提示：你可以假设 nums 中的所有元素是不重复的，n 将在 [1, 10000]之间，nums 的每个元素都将在 [-9999, 9999]之间。
 * [-1,0,3,5,9,12]  查询9
 */
public class Erfenchazhao704 {
    public static void main(String[] args) {
        int[] nums = new int[]{-1,0,1,2,3,5,9,12};
        int target = 9;
        int search = search(nums, target);
        System.out.println(search);
    }

    /*
     * 普通方式
     * public static int search(int[] nums, int target) {
     *         for (int i = 0; i < nums.length; i++) {
     *             if (nums[i] == target){
     *                 return i;
     *             }
     *         }
     *         return -1;
     *     }
     */


    /**
     * 二分法查找
     */
    public static int search(int[] nums, int target) {
        int left = 0, right = nums.length -1;
        while (left <= right){
            //防止int整数越界
            int mid = (right - left) / 2 + left;
            int num = nums[mid];
            if (num == target){
                return mid;
            }else if (num > target){
                right = mid - 1;
            }else {
                left = mid + 1;
            }
        }
        return -1;
    }
}
