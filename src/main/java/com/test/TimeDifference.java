package com.test;

import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2022/1/18
 */
public class TimeDifference {
    @Test
    public void test1(){
        String date = "20220119";
        Calendar instance = Calendar.getInstance();
        System.out.println(Integer.parseInt(formatDate(instance.getTime(), "yyyyMMdd")) - Integer.parseInt(date));
    }

    public static String formatDate(Date date, String format){
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }
}
