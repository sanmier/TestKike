package com.test;

import com.entity.Book;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author: liweicheng
 * @Date: 2021/7/13 14:15
 */
public class DateTest {

    /**
     * 获取年
     */
    @Test
    public void dateTest(){
        Calendar instance = Calendar.getInstance();
        int year = instance.get(Calendar.YEAR);
        System.out.println(year);
    }


    /**
     * 测试时间显示
     */
    @Test
    public void test1(){
        Book book = new Book();
        Book book1 = new Book(12,"三年高考", new Date());
        Date date = new Date();
        System.out.println(date);
        long time = date.getTime();
        System.out.println(time);
    }

    @Test
    public void test2(){
        String ss = "1212\"1212\"1212";
        System.out.println(ss);
    }

    @Test
    public void test3(){
        Set<String> strings = new HashSet<>();
        strings.add("11");
        strings.add("22");
        strings.add("131");
        //strings.add("11");
        System.out.println(strings);
    }

    @Test
    public void test4(){
        String yyyyMMdd = new SimpleDateFormat("yyyyMMdd").format(new Date());
        System.out.println(yyyyMMdd);
        System.out.println("-----------");
        String yyyyMMdd1 = new SimpleDateFormat("yyyyMMdd").format(new Date()).substring(6);
        System.out.println(yyyyMMdd1);
        System.out.println("-----------");
        int a = Integer.parseInt(new SimpleDateFormat("yyyyMMdd").format(new Date()).substring(6));
        System.out.println(a);
        if (a == 20){
            System.out.println("试验结束");
        }
    }

    @Test
    public void test5(){
        String s = "T+13（自然日）";
        if (s.contains("自然日")){
            s=s.replace("（自然日）","");
            s=s.replace("(自然日)","");
        }
        int i = Integer.parseInt(s.substring(2, s.length()));
        System.out.println(i);
        System.out.println("---------");
        String ss = "20220121";
        if (timeDifference(ss) < 0){
            System.out.println("victory");
        }
    }
    private int timeDifference(String date){
        Calendar instance = Calendar.getInstance();
        return Integer.parseInt(formatDate(instance.getTime(),"yyyyMMdd")) - Integer.parseInt(date);
    }

    private String formatDate(Date date,String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    @Test
    public void test6(){
        Date date = new Date();
        SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(Calendar.MONTH, -1);
        instance.set(Calendar.DATE, instance.getActualMaximum(Calendar.DATE));
        Date time = instance.getTime();
        String format = yyyyMMdd.format(time);
        System.out.println(format);
    }
}
