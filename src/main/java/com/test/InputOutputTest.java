package com.test;

import org.junit.jupiter.api.Test;

import java.io.*;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2022/1/19
 */
public class InputOutputTest {


    @Test
    public void test1() throws IOException {
        String filePath = "D:\\code\\testcode\\TestKike\\1.txt";
        BufferedWriter bw= new BufferedWriter (new OutputStreamWriter(new FileOutputStream(filePath,true),"GB2312"));
        bw.write("Hello China");
    }
}
