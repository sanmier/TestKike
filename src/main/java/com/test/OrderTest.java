package com.test;

import com.entity.TagDTO;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2024/4/27
 */
public class OrderTest {
    public int tagOrder(List<TagDTO> tagList){
        List<TagDTO> resultList = new ArrayList<>();
        List<TagDTO> preList = new ArrayList<>();
        //排序处理
        Comparator<TagDTO>comparator = Comparator.comparing(TagDTO::getAffection)
                .thenComparing(TagDTO::getRelatedAmount);
        tagList = tagList.stream().sorted(comparator.reversed()).collect(Collectors.toList());
        //前六都是正向
        if (tagList.size() >= 6 && tagList.get(5).getAffection() > 0){
            TagDTO dto = new TagDTO();
            for (int i = 0; i < tagList.size(); i++) {
                if (tagList.get(i).getAffection() < 0){
                    dto = tagList.get(i);
                    break;
                }
                if (preList.size() < 5){
                    preList.add(tagList.get(i));
                }
            }
            //源列表去除负向中最高
            tagList.remove(dto);
            //源列表去除前五位，得到后段列表
            tagList.removeAll(preList);
            //组装前六位排序
            preList.add(dto);
            List<TagDTO> collect = preList.stream().sorted(Comparator.comparing(TagDTO::getRelatedAmount).reversed()).collect(Collectors.toList());
            //组装结果列表
            resultList.addAll(collect);
            resultList.addAll(tagList);
        }
        int result = 0;
        for (int i = 0; i < resultList.size(); i++) {
            if (resultList.get(i).getTagId() == 1 || resultList.get(i).getTagId() == 2){
                result += i + 1;
            }
        }
        return result;
    }
}

