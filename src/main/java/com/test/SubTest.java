package com.test;

import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/11/18
 */
public class SubTest {

    @Test
    public void test1(){

        String s = "123.";

        s = s.substring(3, s.length());
        System.out.println(s);


        String ss = "20211011";
        Date parse = null;
        DateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");
        try {
            parse = yyyyMMdd.parse(ss);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(parse);
    }
}
