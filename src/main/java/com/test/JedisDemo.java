package com.test;

import redis.clients.jedis.Jedis;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2022/4/13
 */
public class JedisDemo {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        System.out.println("connection is OK ======> : " + jedis.ping());
    }
}
