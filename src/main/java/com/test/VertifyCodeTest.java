package com.test;

import redis.clients.jedis.Jedis;

import java.util.Random;

/**
 * 验证码
 * 1、输入手机号，点击发送后随机生成 6 位数字码，2 分钟有效；
 * 2、输入验证码，点击验证，返回成功或失败；
 * 3、每个手机号每天只能输入 3 次。
 * @author Sanmier
 * @version 1.0
 * @date 2022/4/12
 */
public class VertifyCodeTest {

    //1、生成一个6位的数字验证码
    public static String getCode(){
        Random random = new Random();
        String yanZhengCode = "";
        for (int i = 0; i < 6; i++) {
            int nextInt = random.nextInt(10);
            yanZhengCode += nextInt;
        }
        return yanZhengCode;
    }

    //2、每个手机每天只能发3次，验证码放到redis中，设置过期日期
    public static void vertifyCode(String phone){
        //创建一个Jedis对象
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        //拼接key
        //手机发送次数key
        String countKey = "VertifyCode" + phone + ":count";
        //验证码key
        String codeKey = "VertifyCode" + phone + ":code";

        //每个手机每天只能发送三次
        String count = jedis.get(countKey);
        if (count == null){
            //没有次数 还能发送三次 此次为第一次发送 设置发送次数为1
            jedis.setex(countKey, 24*60*60, "1");
        }else if (Integer.parseInt(count) <= 2){
            //发送次数 +1
            jedis.incr(countKey);
        }else if (Integer.parseInt(count) > 2){
            //发送三次了  不能在发送了
            System.out.println("今日获取验证码已达上限!");
            jedis.close();
            return;
        }

        //发送的验证码放到redis里面
        String vcode = getCode();
        jedis.setex(codeKey, 120, vcode);
        jedis.close();
    }

    //3、验证码校验
    public static void getRedisCode(String phone, String code){
        //先从redis中获取验证码
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        //验证码key
        String codeKey = "VertifyCode" + phone + ":code";
        String redisCode = jedis.get(codeKey);
        //判断
        if (redisCode.equals(code)){
            System.out.println("验证成功！");
        }else {
            System.out.println("验证码不一致！");
        }
    }

    public static void main(String[] args) {
        //模拟验证码的发送
        //vertifyCode("17809298555");
        getRedisCode("17809298555", "6759");
    }
}
