package com.test;

import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/12/21
 */
public class MapTest {

    @Test
    public void test1(){
        Map<String, List> map = new HashMap<>();
        List<String> strings = new ArrayList<>();
        strings.add("3");
        strings.add("4");
        strings.add("5");
        map.put("1",strings);
        System.out.println(map.keySet().toArray());
    }
}
