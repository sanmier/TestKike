package com.test;

import org.junit.jupiter.api.Test;

import java.util.Scanner;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/12/3
 */
public class ScannerTest {

    public static void main(String[] args) {
        test1();
    }

    public static void test1(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Now,Who is joker of the banjitino?");
        int inputs = scanner.nextInt();
        double score;
        int sum = 0;
        for (int i = 0; i < inputs; i++) {
            System.out.println("请输入第" + (i + 1) + "位学生的成绩：");
            score = scanner.nextDouble();
            if (score >= 80){
                sum ++ ;
            }
        }
        System.out.println("80分以上的学生人数是：" + sum);
        System.out.println("80分以上的學生所占比例為：" + ((double) sum / (double) inputs) * 100 + "%");
    }

    @Test
    public void test2(){
        String s = "11";
        s = "22";
        s = "33";
        System.out.println(s);

    }

    @Test
    public void test3(){

    }
}
