package com.test;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/11/22
 */

public class RollBackTest {

    public static void main(String[] args) {
        String s = test1();
        System.out.println(s);
    }

    private static String test1(){
        StringBuilder stringBuilder = new StringBuilder();
        String s = "";
        int i = 1;
        try {
            int a = 1/0;
        } catch (Exception e) {
             s = "0不能当分母！";
        }
        i++;
        return s;
    }
}
