package com.test;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/11/25
 */
public class StreamTest {

    @Test
    public void test1(){
        ArrayList<String> strings = new ArrayList<>();
        strings.add("1");
        strings.add("2");
        strings.add("3");
        strings.add("2");
        strings.add("6");
        System.out.println(strings);

//        List<String> collect = strings.stream().distinct().collect(Collectors.toList());
        List<String> collect = strings.stream()
                .filter(string -> !"2".equals(string) && !string.equals("3"))
                .collect(Collectors.toList());
        System.out.println(collect);
    }

    @Test
    public void test2(){
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        for (int i = 0; i < 5; i++) {
            HashMap<String, String> map = new HashMap<>();
            list.add(map);
            list.get(i).put("1",String.valueOf(i));
        }
        System.out.println(list);

//        List<String> collect = strings.stream().distinct().collect(Collectors.toList());
        list = list
                .stream()
                .map(map -> {
                    //map.entrySet().stream().filter(string -> !"2".equals(map.get("1")));
                        if("2".equals(map.get("1"))){
                        return null;
                    }
                    return map;
                }).filter(l -> l != null).collect(Collectors.toList());
        System.out.println(list);
    }

    @Test
    public void test3(){
        List<String> list = new ArrayList<>();
        list.add("123");
        list.add("345");
        list.add("567");
        List<String> collect = list.stream().distinct().collect(Collectors.toList());
        System.out.println(collect);
    }

}
