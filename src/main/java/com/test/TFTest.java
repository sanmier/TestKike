package com.test;

import org.junit.jupiter.api.Test;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/11/18
 */
public class TFTest {

    @Test
    public void test1(){
        boolean flag = true;
        boolean flag1 = false;
        boolean flag2 = true;
        boolean flag3 = false;

        boolean q = flag || flag1;
        System.out.println(q);

        q = flag1 || flag;
        System.out.println(q);

        q = flag2 || flag;
        System.out.println(q);

        q = flag1 || flag3;
        System.out.println(q);

        System.out.println("---------------------");

        q = flag && flag1;
        System.out.println(q);

        q = flag && flag2;
        System.out.println(q);

        q = flag3 && flag;
        System.out.println(q);
    }

}
