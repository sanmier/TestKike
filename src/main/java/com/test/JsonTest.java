package com.test;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/12/1
 */
public class JsonTest {
    @Test
    public void test1() {
        String ss = "[{\"code\" : \"13051237766\",\"dictTypeId\" : \"iam-rhzg-phones\",\"name\" : \"姚亮\",\"tenantId\" : \"default\"},{\"code\": \"18232447980\",\"dictTypeId\" : \"iam-rhzg-phones\",\"name\" : \"焦文儒\",\"tenantId\" : \"default\"},{\"code\" : \"18033871992\",\"dictTypeId\" : \"iam-rhzg-phones\",\"name\" : \"郭峻蕊\",\"tenantId\" : \"default\"},{\"code\" :\"13159802157\",\"dictTypeId\" : \"iam-rhzg-phones\",\"name\" : \"李维成\",\"description\" : \"\",\"tenantId\" : \"default\"}]";
        JSONArray objects = new JSONArray(ss);
        List<String> code = objects.stream()
                .map(m -> ((JSONObject) m).getStr("code"))
                .distinct()
                .collect(Collectors.toList());
        System.out.println(code);
    }


    @Test
    public void test2(){
        String s = "";
        String s1 = "";
        String s2 = "";
        String s3 = "";
        String s4 = s + s1 + s2 + s3;
        //""
        System.out.println(s4);
        //true
        System.out.println(s3.equals(s4));
        System.out.println("-------------------");
        //false
        System.out.println(s3 == s4);

    }
}
