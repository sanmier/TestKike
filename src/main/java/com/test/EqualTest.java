package com.test;

import com.entity.Book;
import org.junit.jupiter.api.Test;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/12/22
 */
public class EqualTest {

    @Test
    public void test1(){
        String s = "1";
        if ("1".equals(s)){
            System.out.println("老子等于一");
        }else if ("2".equals(s)){
            System.out.println("老子等于二");
        }else if (!"".equals(s) && s != null){
            System.out.println("老子不为空");
        }
    }

    //判断字段都为空的对象是否为空对象
    @Test
    public void test2(){
        Book book = new Book();
        book.setName(null);
        book.setPublishDt(null);
        System.out.println(book);

        if (book == null){
            System.out.println("字段都为空的对象是空对象!!!");
        }
    }

}
