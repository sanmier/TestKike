package com.test;

import org.junit.jupiter.api.Test;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/12/29
 */
public class CharTest {

    @Test
    public void test1(){
        String s = "0123456789";
        for (byte aByte : s.getBytes()) {
            if (aByte > 57 || aByte < 48){
                System.out.println("你有问题!");
            }else {
                System.out.println("没事了");
            }
        }
    }

    public static void main(String[] args) throws NoSuchMethodException {
        String ss = "test2";
        CharTest.class.getMethod(ss);
    }


    private void test2(String s) {
        System.out.println("这个是重载！");
    }

    private void test2(){
        System.out.println("这个不是重载！");
    }
}
