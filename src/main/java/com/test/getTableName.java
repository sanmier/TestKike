package com.test;

import com.entity.Book;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;



/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/12/16
 */
public class getTableName {

    /**
     * 获取类上注解value值从而获取表名
     */
    @SneakyThrows
    @Test
    public void test1(){
        Book book = new Book();
//        Class<?> bookClass = book.getClass();
//        TableName annotation = bookClass.getAnnotation(TableName.class);
//        String value = annotation.value();
//        System.out.println(value);

        //获取类上注解value值从而获取表名
//        System.out.println(bookClass);
//        String name = bookClass.getName();
//        System.out.println(name);

        //根据注解的value值获取实体类
        String tableName = "bOoK";
        tableName = tableName.toUpperCase();
        String[] split = tableName.split("_");
        String newTableName = new String();
        for (String section : split) {
            section = section.charAt(0) + section.substring(1).toLowerCase();
            newTableName += section;
        }
//        System.out.println(newTableName);
        //以String类型的className实例化类
        Class obj=Class.forName("com.entity." + newTableName);
        Object o = obj.newInstance();
        System.out.println(obj.getSimpleName());
        System.out.println(obj);
//        System.out.println(obj.getAnnotation(TableName.class));

    }
}
