package com.test;

import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/11/18
 */
public class ClassAndMethodTest {

    @Test
    public void test1(){
        String s = this.getClass().getName();
        String className = s.substring(s.lastIndexOf(".") + 1, s.length());
        String methodName = new Exception().getStackTrace()[0].getMethodName();
        System.out.println(methodName);
    }

    @Test
    public void test2(){
        Calendar instance = Calendar.getInstance();
        Date time = instance.getTime();
        System.out.println(time);
    }
}
