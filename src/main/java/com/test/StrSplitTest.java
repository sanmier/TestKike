package com.test;

import com.entity.Book;
import com.entity.Person;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: liweicheng
 * @Date: 2021/7/13 14:15
 */
public class StrSplitTest {

    @Test
    public void strSplitTest(){
        Person p1 = new Person();
        p1.setName("张三");
        Person p2 = new Person();
        p2.setName("李四");
        Person p3 = new Person();
        p3.setName("王五");
        List<Person> ss = new ArrayList<>();
        ss.add(p1);
        ss.add(p2);
        ss.add(p3);

//        //拼接方式一
//        String ns = new String();
//        int count = 1;
//        for(Person s : ss){
//            if (count == ss.size()){
//                ns = ns.concat(s.getName());
//
//            }
//            if (count < ss.size()){
//                ns = ns.concat(s.getName() + ",");
//            }
//            count++;
//        }
//        System.out.println(ns.trim());

        //拼接方式二
        StringBuffer sb = new StringBuffer();
        for(Person s : ss){
            sb = sb.append(s.getName()).append(",");
        }
        System.out.println(sb.deleteCharAt(sb.length()-1));
    }

    @Test
    public void nullStr(){
        String str = "abcdef";
        str = str.replace("b","");
        //acdef
        System.out.println(str);
        //acf
        str = str.replace("de","");
        System.out.println(str);

        str = str.replace("a","c");
        System.out.println(str);
    }


    @Test
    public void test1(){
        StringBuilder stringBuilder = new StringBuilder();
        System.out.println(stringBuilder.length());
        System.out.println("--------------");
        stringBuilder.append("1");
        System.out.println(stringBuilder.toString());
        System.out.println(stringBuilder.length());
        System.out.println("--------------");
        stringBuilder.append("2");
        System.out.println(stringBuilder.length());
        System.out.println(stringBuilder.toString());
        stringBuilder.append("4");
        System.out.println("--------------");
        System.out.println(stringBuilder);
        System.out.println(stringBuilder.toString());
        System.out.println(stringBuilder.length());
        Book book = new Book();
        System.out.println(book);

    }

    @Test
    public void test2(){
        String s = "1234561217";
//        s = s.substring(2, s.length());
//        int i = s.indexOf("1");
        String substring = s.substring(s.length()-1);
        System.out.println(substring);
    }

    @Test
    public void test3(){
        String s = "T+3自然日";
        System.out.println(s.length());
        if (s.contains("自然日")){
            s = s.replace("自然日","");
        }
        System.out.println(s);
        System.out.println(s.length());
    }

    @Test
    public void test4(){
        String s = "2021-01-03";
        String replace = s.replace("-", "");
        System.out.println(replace);
    }
}
