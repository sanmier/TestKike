package com.test;

import java.util.concurrent.Callable;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2024/4/22
 */
public class ThreadCreateTest {
    public static void main(String[] args) {
        //Thread thread = new Thread(new MyThread());
        //thread.start();
        Thread thread = new Thread(new MyRunnable1());
        thread.start();
        //FutureTask<Integer> futureTask = new FutureTask<>(new MyCallable());
        //Thread thread = new Thread(futureTask);
        //thread.start();
    }
}

class MyCallable implements Callable<Integer>{

    @Override
    public Integer call() throws Exception {
        try {
            System.out.println("通过实现Callable创建线程！！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 100;
    }
}

class MyRunnable1 implements Runnable{

    @Override
    public void run() {
        try {
            int i = 1/0;
        } catch (Exception e) {
            System.out.println("抓到异常");
            e.printStackTrace();
        }
    }
}

class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("通过实现Runnable创建线程！！");
    }
}


class MyThread extends Thread{
    @Override
    public void run(){
        System.out.println("通过继承thread创建线程！！");
    }
}
