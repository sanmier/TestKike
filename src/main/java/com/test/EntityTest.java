package com.test;

import com.entity.Book;
import org.junit.jupiter.api.Test;
import java.util.Date;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2022/3/14
 */
public class EntityTest {

    @Test
    public void test1(){
        Book book = new Book();
        book.setName("ad");
        book.setPublishDt(new Date());
        book.setId(9);
        System.out.println(book);
    }
}
