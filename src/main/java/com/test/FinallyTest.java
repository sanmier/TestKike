package com.test;

/**
 *  finally 里面的 return 覆盖 catch 中 return
 */
public class FinallyTest {

    public static void main(String[] args) {
        String s = testFinally();
        System.out.println(s);
    }

    private static String testFinally() {
        try {
//            int a = 1/2;
            int a = 1/0;
            return "try出";
        }catch (Exception e){
            return "catch出";
        }finally {
            System.out.println("finally的sout出");
            return "finally出";
        }
    }
}
