package com.test.singleton;

/**
 * @author Sanmier
 * @version 1.0
 * @date 2021/11/12
 * 在内部类被加载和初始化时候  才创建instance实例对象
 * 静态内部类不会自动碎着外部类的加载和初始化而初始化，他是单独去加载和初始化的。
 * 因为是内部类加载和初始化的，因此线程是安全的。
 */
public class Singleton6 {

    private Singleton6(){

    }
    private static class Inner{
        private static final Singleton6 INSTANCE = new Singleton6();
    }

    public static Singleton6 getInstance(){
        return Inner.INSTANCE;
    }
}
